import axios from 'axios';
import { setAlert } from './alert';
import * as types from './types';
const _ = require('lodash');

// GET THE CURRENT USER'S PROFILE
export const getCurrentProfile = () => async dispatch => {
  try {
    const res = await axios.get(`/api/profile/me`);
    dispatch({
      type: types.GET_PROFILE,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: types.PROFILE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

export const getProfiles = () => async dispatch => {
  dispatch({ type: types.CLEAR_PROFILE });
  try {
    const res = await axios.get(`/api/profile`);
    dispatch({
      type: types.GET_PROFILES,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: types.PROFILE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

export const getProfileById = userId => async dispatch => {
  try {
    const res = await axios.get(`/api/profile/user/${userId}`);
    dispatch({
      type: types.GET_PROFILE,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: types.PROFILE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

export const getGithubRepos = username => async dispatch => {
  try {
    const res = await axios.get(`/api/profile/github/${username}`);
    dispatch({
      type: types.GET_REPOS,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: types.PROFILE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

export const saveProfile = (
  formData,
  history,
  edit = false
) => async dispatch => {
  try {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    };

    const res = await axios.post('/api/profile', formData, config);
    dispatch({
      type: types.GET_PROFILE,
      payload: res.data
    });
    dispatch(setAlert(edit ? 'Profile updated' : 'Profile created', 'success'));
    if (!edit) {
      history.push('/dashboard');
    }
  } catch (err) {
    const errors = err.response.data.errors;
    if (errors) {
      _.forEach(errors, function(error) {
        dispatch(setAlert(error.msg, 'danger', 5000));
      });
    }
    dispatch({
      type: types.PROFILE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

export const addExperience = (formData, history) => async dispatch => {
  try {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    };

    const res = await axios.put('/api/profile/experience', formData, config);
    dispatch({
      type: types.GET_PROFILE,
      payload: res.data.profile
    });
    dispatch(setAlert('Experience Added', 'success'));
    history.push('/dashboard');
  } catch (err) {
    const errors = err.response.data.errors;
    if (errors) {
      _.forEach(errors, function(error) {
        dispatch(setAlert(error.msg, 'danger', 5000));
      });
    }
    dispatch({
      type: types.PROFILE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

export const addEducation = (formData, history) => async dispatch => {
  try {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    };

    const res = await axios.put('/api/profile/education', formData, config);
    dispatch({
      type: types.GET_PROFILE,
      payload: res.data.profile
    });
    dispatch(setAlert('Education Added', 'success'));
    history.push('/dashboard');
  } catch (err) {
    const errors = err.response.data.errors;
    if (errors) {
      _.forEach(errors, function(error) {
        dispatch(setAlert(error.msg, 'danger', 5000));
      });
    }
    dispatch({
      type: types.PROFILE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

export const deleteExperience = id => async dispatch => {
  try {
    const res = await axios.delete(`/api/profile/experience/${id}`);

    dispatch({
      type: types.UPDATE_PROFILE,
      payload: res.data
    });
    dispatch(setAlert('Experience Removed', 'success'));
  } catch (err) {
    dispatch({
      type: types.PROFILE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

export const deleteEducation = id => async dispatch => {
  try {
    const res = await axios.delete(`/api/profile/education/${id}`);

    dispatch({
      type: types.UPDATE_PROFILE,
      payload: res.data
    });
    dispatch(setAlert('Education Removed', 'success'));
  } catch (err) {
    dispatch({
      type: types.PROFILE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

export const deleteAccount = () => async dispatch => {
  if (window.confirm('Are you sure? This action cannot be undone')) {
    try {
      await axios.delete(`/api/profile`);

      dispatch({
        type: types.CLEAR_PROFILE
      });
      dispatch({
        type: types.ACCOUNT_DELETED
      });
      dispatch(setAlert('Your account has been deleted'));
    } catch (err) {
      dispatch({
        type: types.PROFILE_ERROR,
        payload: { msg: err.response.statusText, status: err.response.status }
      });
    }
  }
};
