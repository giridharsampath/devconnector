import * as types from '../actions/types';
import { setAlert } from './alert';
import axios from 'axios';

export const getPosts = () => async dispatch => {
  try {
    const res = await axios.get('/api/posts');
    dispatch({ type: types.GET_POSTS, payload: res.data });
  } catch (err) {
    dispatch({
      type: types.POST_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

export const addLike = id => async dispatch => {
  try {
    const res = await axios.put(`/api/posts/${id}/like`);
    dispatch({
      type: types.UPDATE_LIKES,
      payload: {
        id,
        likes: res.data
      }
    });
  } catch (err) {
    dispatch({
      type: types.POST_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

export const removeLike = id => async dispatch => {
  try {
    const res = await axios.put(`/api/posts/${id}/unlike`);
    dispatch({
      type: types.UPDATE_LIKES,
      payload: {
        id,
        likes: res.data
      }
    });
  } catch (err) {
    dispatch({
      type: types.POST_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

export const deletePost = id => async dispatch => {
  if (window.confirm('Are you sure?')) {
    try {
      const res = await axios.delete(`/api/posts/${id}`);
      dispatch({
        type: types.DELETE_POST,
        payload: {
          id
        }
      });
      dispatch(setAlert('Post has been removed', 'success'));
    } catch (err) {
      dispatch({
        type: types.POST_ERROR,
        payload: { msg: err.response.statusText, status: err.response.status }
      });
    }
  }
};

export const addPost = formData => async dispatch => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };
  try {
    const res = await axios.post(`/api/posts`, formData, config);
    dispatch({
      type: types.ADD_POST,
      payload: res.data
    });
    dispatch(setAlert('Post has been added', 'success'));
  } catch (err) {
    dispatch({
      type: types.POST_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

export const addComment = (id, formData) => async dispatch => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };
  try {
    const res = await axios.post(`/api/posts/${id}/comments`, formData, config);
    dispatch({
      type: types.ADD_COMMENT,
      payload: res.data
    });
    dispatch(setAlert('Comment has been added', 'success'));
  } catch (err) {
    dispatch({
      type: types.POST_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

export const removeComment = (id, commentId) => async dispatch => {
  try {
    await axios.delete(`/api/posts/${id}/comments/${commentId}`);
    dispatch({
      type: types.REMOVE_COMMENT,
      payload: commentId
    });
    dispatch(setAlert('Comment has been removed', 'success'));
  } catch (err) {
    dispatch({
      type: types.POST_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

export const getPost = id => async dispatch => {
  try {
    const res = await axios.get(`/api/posts/${id}`);
    dispatch({ type: types.GET_POST, payload: res.data });
  } catch (err) {
    dispatch({
      type: types.POST_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};
