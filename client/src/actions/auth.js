import axios from 'axios';
import * as types from './types';
import { setAlert } from './alert';
import setAuthToken from '../utils/setAuthToken';
const _ = require('lodash');

// LOAD USER

export const loadUser = () => async dispatch => {
  if (localStorage.token) {
    setAuthToken(localStorage.token);
  }

  try {
    const res = await axios.get('/api/auth');
    dispatch({
      type: types.USER_LOADED,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: types.AUTH_ERROR
    });
  }
};

// REGISTER USER
export const register = ({ name, email, password }) => async dispatch => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };
  const body = JSON.stringify({ name, email, password });
  try {
    const res = await axios.post('/api/users', body, config);
    dispatch({
      type: types.REGISTER_SUCCESS,
      payload: res.data
    });
    dispatch(loadUser());
  } catch (err) {
    console.error(err);
    const errors = err.response.data.errors;
    if (errors) {
      _.forEach(errors, function(error) {
        dispatch(setAlert(error.msg, 'danger', 5000));
      });
    }
    dispatch({
      type: types.REGISTER_FAIL
    });
  }
};

// LOGIN USER
export const login = (email, password) => async dispatch => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };
  const body = JSON.stringify({ email, password });
  try {
    const res = await axios.post('/api/auth', body, config);
    dispatch({
      type: types.LOGIN_SUCCESS,
      payload: res.data
    });
    dispatch(loadUser());
  } catch (err) {
    const errors = err.response.data.errors;
    if (errors) {
      _.forEach(errors, function(error) {
        dispatch(setAlert(error.msg, 'danger', 5000));
      });
    }
    dispatch({
      type: types.LOGIN_FAIL
    });
  }
};

export const logout = () => async dispatch => {
  dispatch({ type: types.CLEAR_PROFILE });
  dispatch({ type: types.LOGOUT });
};
