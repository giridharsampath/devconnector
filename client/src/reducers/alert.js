import * as types from '../actions/types';
const initialState = [];
const _ = require('lodash');

export default function(state = initialState, action) {
  const { type } = action;
  const { payload } = action;
  switch (type) {
    case types.SET_ALERT:
      return [...state, payload];
    case types.REMOVE_ALERT:
      return _.filter(state, alert => alert.id !== payload);
    default:
      return state;
  }
}
